import copy, random
from .cell import *
from .user_input import *

def numbers(size):
	labels = [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 ", 
			  " A ", " B ", " C ", " D ", " E ", " F ", " G ", " H ", " I ", 
			  " J ", " K ", " L ", " M ", " N ", " O ", " P ", " Q ", " R ",
			  " S ", " T ", " U ", " V ", " W ", " X ", " Y ", " Z "]
	r = ["---", "   ", "   ", "---"]
	r += labels[0:size]
	r += ["---",  "   ",  "---"]
	return r

def gap(t, m, size):
	r = [t, m, m, t]
	for k in range(size):
		r += [m]
	r += [t, m, t]
	return r

def report_board(board,msg,isp,size,labels):
	d = "-"
	b = " "
	n = " "
	labels = labels[4:size+4]
	for j in range(size):
		d += "--"
		b += "  "
		n += str(labels[j].strip()) + " "
	r = [d, msg, n, d]
	for k in range(size):
		s = " "
		if board == None:
			r += [b]
		else:
			for j in range(size):
				c = board[k][j]
				if isp:
					s += report_player_cell(c)
				else:
					s += report_computer_cell(c)
				s += " "			
			r += [s]
	r += [d, n, d]
	return r
	
def report_player_board(board, size):
	x=' ';
	spacing = (size*2+1) - 6;
	return report_board(board,(spacing/2)*x + "PBoard" + ((spacing/2)+1)*x,True, size, numbers(size))
	
def report_computer_board(board, size):
	x=' ';
	spacing = (size*2+1) - 6;
	return report_board(board,(spacing/2)*x + "CBoard" + ((spacing/2)+1)*x,False, size, numbers(size))

def pad(s):
	r = " " + s
	while len(r) < 19:
		r += " "
	# print("<"+s+"><"+r+">")
	return r
	
def report_ships(board, ships, msg, req):
	r = [pad(msg)]
	# print("++" + msg)
	# print("Ships: ", ships)
	# print("Board: ", board)
	for s in ships:
		# print("&" + s)
		h = board[-1][s]
		if req == None or req == (h > 0):
			r += [pad("  " + s)]
	r += ["                   "]
	return r
		
def report_fleet(board,ships,msg,size):
	d = "-------------------"
	b = "                   "
	r = [d, msg, b, d]
	if board != None:
		r += report_ships(board, ships, "Floating", True)
		r += report_ships(board, ships, "Sunk", False)
	while (len(r) < size+4):
		r += [b]
	r += [d, b, d]
	return r
	
def print_all(pboard,cboard,ships,size):
	# David's modified version
	g = gap("+","|", size)
	n = numbers(size)
	bp = report_player_board(pboard,size)
	bc = report_computer_board(cboard,size)
	# next two lines need to be fixed
	sp = report_fleet(pboard,ships,"   Player Fleet    ", size)
	sc = report_fleet(cboard,ships,"  Computer Fleet   ", size)
	# print("g" + str(len(g)) + " n" + str(len(n)) + " bp" + str(len(bp)) + " bc" + str(len(bc)) + " sp" + str(len(sp)) + " sc" + str(len(sc)))
	
	for k in range(0,7+size):
		s = g[k] + n[k] + g[k] + bp[k] + g[k] + n[k] + g[k] + bc[k] + g[k] + n[k] + g[k] + sp[k] + g[k] + sc[k] + g[k]
		print(s)
	# print("<Done>")

def check_sink(board,x,y):
	#figure out what ship was hit
	if board[x][y] == "A":
		ship = "Aircraft Carrier"
	elif board[x][y] == "B":
		ship = "Battleship"
	elif board[x][y] == "S":
		ship = "Submarine" 
	elif board[x][y] == "D":
		ship = "Destroyer"
	elif board[x][y] == "P": 
		ship = "Patrol Boat"
	#mark cell as hit and check if sunk
	board[-1][ship] -= 1
	if board[-1][ship] == 0:
		print ship + " Sunk"

def check_win(board,size):
	#simple for loop to check all cells in 2d board
	#if any cell contains a char that is not a hit or a miss return false
	for i in range(size):
		for j in range(size):
			if board[i][j] != -1 and board[i][j] != '*' and board[i][j] != '$':
				return False
	return True


def user_place_ships(board,ships, size):
	for ship in ships.keys():
		#get coordinates from user and validate the postion
		valid = False
		while(not valid):
			# print_board("u",board)
			print_all(board, None, ships, size)
			print "Placing a " + ship
			x,y = get_coor(size)
			ori = v_or_h()
			valid = validate(board,ships[ship],x,y,ori, size)
			if not valid:
				print "Cannot place a ship there.\nPlease take a look at the board and try again."
				# raw_input("Hit ENTER to continue")
		#place the ship
		board = place_ship(board,ships[ship],ship[0],ori,x,y)
		# print_board("u",board)
		print_all(board, board, ships, size)
	# raw_input("Done placing user ships. Hit ENTER to continue")
	return board

def computer_place_ships(board,ships,size):
	for ship in ships.keys():
		#generate random coordinates and validate the postion
		valid = False
		while(not valid):
			x = random.randint(1,size)-1
			y = random.randint(1,size)-1
			o = random.randint(0,1)
			if o == 0: 
				ori = "v"
			else:
				ori = "h"
			valid = validate(board,ships[ship],x,y,ori,size)

		#place the ship
		print "Computer placing a/an " + ship
		board = place_ship(board,ships[ship],ship[0],ori,x,y)
	return board

def place_ship(board,ship,s,ori,x,y):
	#place ship based on orientation
	if ori == "v":
		for i in range(ship):
			board[x+i][y] = s
	elif ori == "h":
		for i in range(ship):
			board[x][y+i] = s
	return board
	
def validate(board,ship,x,y,ori,size):
	#validate the ship can be placed at given coordinates
	if ori == "v" and x+ship > size:
		return False
	elif ori == "h" and y+ship > size:
		return False
	else:
		if ori == "v":
			for i in range(ship):
				if board[x+i][y] != -1:
					return False
		elif ori == "h":
			for i in range(ship):
				if board[x][y+i] != -1:
					return False
	return True

			
def make_move(board,x,y):
	
	#make a move on the board and return the result, hit, miss or try again for repeat hit
	if board[x][y] == -1:
		return "miss"
	elif board[x][y] == '*' or board[x][y] == '$':
		return "try again"
	else:
		return "hit"

def user_move(board,size):
	
	#get coordinates from the user and try to make move
	#if move is a hit, check ship sunk and win condition
	while(True):
		x,y = get_coor(size)
		res = make_move(board,x,y)
		if res == "hit":
			print "Hit at " + str(x+1) + "," + str(y+1)
			check_sink(board,x,y)
			board[x][y] = '$'
			if check_win(board,size):
				return "WIN"
		elif res == "miss":
			print "Sorry, " + str(x+1) + "," + str(y+1) + " is a miss."
			board[x][y] = "*"
		elif res == "try again":
			print "Sorry, that coordinate was already hit. Please try again"	

		if res != "try again":
			return board

def computer_move(board,size,moveNum):
	
	#generate user coordinates from the user and try to make move
	#if move is a hit, check ship sunk and win condition
	while(True):
		x = random.randint(1,size)-1
		y = random.randint(1,size)-1
		res = make_move(board,x,y)
		if res == "hit":
			print "Hit at " + str(x+1) + "," + str(y+1)
			check_sink(board,x,y)
			board[x][y] = '$'
			if check_win(board,size):
				return "WIN"
		elif res == "miss":
			print "Sorry, " + str(x+1) + "," + str(y+1) + " is a miss."
			board[x][y] = "*"
		elif res == "try again":
			print "Sorry, that coordinate was already hit. Please try again"

		if res != "try again":
			return board
	

