from .board import *
from .cell import *

def v_or_h():
	#get ship orientation from user
	while(True):
		user_input = raw_input("vertical or horizontal (v,h) ? ")
		if user_input == "v" or user_input == "h":
			return user_input
		else:
			print "Invalid input. Please only enter v or h"

def get_coor(size):
	while (True):
		user_input = raw_input("Please enter coordinates (row,col) ? ")
		try:
			#see that user entered 2 values seprated by comma
			coor = user_input.split(",")
			if len(coor) != 2:
				raise Exception("Invalid entry, too few/many coordinates.");

			#check that 2 values are integers
			coor[0] = int(coor[0])-1
			coor[1] = int(coor[1])-1

			#check that values of integers are between 1 and size for both coordinates
			if coor[0] > (size-1) or coor[0] < 0 or coor[1] > (size-1) or coor[1] < 0:
				raise Exception("Invalid entry. Please use values between 1 to " + str(size) + " only.")

			#if everything is ok, return coordinates
			return coor
		
		except ValueError:
			print "Invalid entry. Please enter only numeric values for coordinates"
		except Exception as e:
			print e

def get_grid():
	while (True):
		user_input = raw_input("Please enter the size of the grid (single number between 10 and 35) ")
		try:
			
			#check that value is an integer
			user_input = int(user_input)
			
			#check that values of integers are between 10 and 35 for grid size
			if user_input > 35 or user_input < 10:
				raise Exception("Invalid entry. Please use values between 10 to 35 only.")

			#if everything is ok, return coordinates
			return user_input
		
		except ValueError:
			print "Invalid entry. Please enter only numeric values for size"
		except Exception as e:
			print e

