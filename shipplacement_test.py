from bslib import *
import copy

def main():

	#types of ships
	#ships = {"Battleship":4}
	ships = {"Aircraft Carrier":5, "Battleship":4, "Submarine":3, "Destroyer":3, "Patrol Boat":2}

	#setup blank board (obtains user input for size of grid)
	board = []
	grid_size = get_grid()
	for i in range(grid_size):
		board_row = []
		for j in range(grid_size):
			board_row.append(-1)
		board.append(board_row)

	#setup user and computer boards
	user_board = copy.deepcopy(board)
	comp_board = copy.deepcopy(board)

	#add ships as last element in the array
	user_board.append(copy.deepcopy(ships))
	comp_board.append(copy.deepcopy(ships))

	#ship placement
	user_board = user_place_ships(user_board,ships,grid_size)
	comp_board = computer_place_ships(comp_board,ships,grid_size)

	moveNum = 0;
	#game main loop
	while(1):

		#user move
		# print_board("c",comp_board)
		print_all(user_board,comp_board,ships,grid_size)
		comp_board = user_move(comp_board,grid_size)

		#check if user won
		if comp_board == "WIN":
			print "User WON! :)"
			quit()
			
		#display current computer board
		# print_board("c",comp_board)
		print_all(user_board,comp_board,ships,grid_size)
		# raw_input("To end user turn hit ENTER")

		#computer move
		moveNum += 1
		user_board = computer_move(user_board,grid_size,moveNum)
		
		#check if computer move
		if user_board == "WIN":
			print "Computer WON! :("
			quit()
			
		#display user board
		# print_board("u",user_board)
		print_all(user_board,comp_board,ships,grid_size)
		# raw_input("To end computer turn hit ENTER")
	
if __name__=="__main__":
	main()


