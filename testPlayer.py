import unittest
import Player

class TestPlayer(unittest.TestCase):
	def setUp(self):
		self.player = Player.Player('red', 'redMove.dat')
		
	def test_loader(self):
		self.assertEqual(len(self.player.moves), 4)
		
	def test_getNextMove(self):
		self.player.getNextMove()
		mv2 = self.player.getNextMove()
		self.assertEqual(mv2, ['3','3'])
		more = self.player.hasMoreMove()
		self.assertEqual(more, True)
		self.player.getNextMove()
		self.player.getNextMove()
		more = self.player.hasMoreMove()		
		self.assertEqual(more, False)
		
	def test_FailEndOfLst(self):
		with self.assertRaises(Exception) as cm:
			self.player.getNextMove()
			self.player.getNextMove()
			self.player.getNextMove()
			self.player.getNextMove()
			self.player.getNextMove()
		
if __name__ == '__main__':
    unittest.main()
		
